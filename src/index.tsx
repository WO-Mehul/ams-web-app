import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { applyMiddleware, createStore } from 'redux';
import ReduxPromise from 'redux-promise';
import ReduxThunk from 'redux-thunk';
import App from './App';
import { setSwaggerClient, requestInterceptor, responseInterceptor, init } from './common/API';
import rootReducer from './redux/reducers';
// @ts-ignore
import Swagger from 'swagger-client';
import Config from './config';

const storeWithMiddleware = applyMiddleware(ReduxThunk, ReduxPromise)(
  createStore
);

const store = storeWithMiddleware(
  rootReducer,
  // @ts-ignore-start
  window.__REDUX_DEVTOOLS_EXTENSION__ &&  window.__REDUX_DEVTOOLS_EXTENSION__()
  // @ts-ignore-end
);
init();
Swagger({
  url: `${document.location.protocol}//${Config ? Config.host : ''}:${Config ? Config.port : ''}/swagger`,
  requestInterceptor: requestInterceptor,
  responseInterceptor: responseInterceptor
}).then((swaggerClient: any) => {
    console.log(swaggerClient)
  setSwaggerClient(swaggerClient);
  // assumes that 'schema' already exists in scope and is the schema object for
  // http://petstore.swagger.wordnik.com/api/api-docs

  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <App/>
      </Router>
    </Provider>,
    document.getElementById('wrapper')
  );
});

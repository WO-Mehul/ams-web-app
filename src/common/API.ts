// import { getCookie } from "../Redux/Reducers/AuthReducer";


export const swagger: any = {
    headers: { 'Content-Type': 'application/json' },
    setHeader: (obj: any) => {
        Object.keys(obj).forEach((key) => {
            swagger.headers[key] = obj[key];
        });
    },
    setAuthorization: (user: string, password: string) => {
        const token = user + ':' + password;
        swagger.setHeader({ authorization: `Basic ${btoa(token)}` });
    }
};
export const init = () => {
    if (document.cookie.includes('authentication_token')) {
        // swagger.setHeader({ 'x-access-token': JSON.parse(getCookie('authentication_token')).token });
        // swagger.setHeader({ 'x-super-access-token': JSON.parse(getCookie('authentication_token')).superToken })
    }

};
export const setSwaggerClient = (client: any) => {
    swagger.client = client;
};
export const requestInterceptor = (request: any) => {
    request.headers = swagger.headers;
    return request;
};
export const responseInterceptor = (response: any) => {
    return response;
};


export const errorHandler = (error: any) => {
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.error('Error data', error.response.data);
        console.error('Error status', error.response.status);
        console.error('Error headers', error.response.headers);
    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.error('Error', error.request);
    } else {
        // Something happened in setting up the request that triggered an Error
        console.error('Error', error.message);
    }
};
// SetPrimaryServiceKey(Config.service.id, Config.service.key);

const INITIAL_STATE = {
  sidebar: {
    sideBarOpen: false
  }
};
export default (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case 'SIDEBAR_TOGGLE':
      if (action.payload.sideBarOpen) {
        document.body.classList.add('sidebar-toggled');
      } else {
        document.body.classList.remove('sidebar-toggled');
      }
      return {
        sidebar: {
          sideBarOpen: action.payload.sideBarOpen
        }
      };
    default:
      return state;
  }
};

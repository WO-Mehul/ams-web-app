import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import AuthReducer from './AuthReducer';
import NavigationReducer from './NavigationReducer';

const rootReducer = combineReducers({
  form: formReducer,
  auth: AuthReducer,
  navigation: NavigationReducer
});

export default rootReducer;

import { LOGIN, SIGN_OUT } from '../actions';
import Config from '../../config';

const initAuth = () => {
  if (getCookie('authentication_token')) {
    return JSON.parse(getCookie('authentication_token'));
  }
  if (getCookie('authentication_token') && Config.debug) {
    const obj = JSON.parse(getCookie('authentication_token'));
    return obj;
  } else {
    return {
      'token': null
    };
  }
};
export default (auth = initAuth(), action: any) => {
  let obj = null;
  switch (action.type) {
    case LOGIN:
      obj = action.payload;
      if (Config.debug) {
        setCookie('authentication_token', JSON.stringify(obj));
      }
      return obj;
    case SIGN_OUT:
      deleteAllCookies();
      return obj;
    default:
      return auth;
  }
};
const setCookie = (cname: any, cvalue: any) => {
  const d = new Date();
  d.setTime(d.getTime() + (60 * 60 * 1000));
  const expires = 'expires=' + d.toUTCString();
  document.cookie = cname + '=' + btoa(cvalue) + ';' + expires + ';path=/';
};

export const getCookie = (cname: any) => {
  const name = cname + '=';
  const decodedCookie = decodeURIComponent(document.cookie);
  const ca = decodedCookie.split(';');
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return atob(c.substring(name.length, c.length));
    }
  }
  return '';
};

function deleteAllCookies() {
  const c: any = document.cookie.split('; ');
  c.forEach((d: any) => {
    // eslint-disable-next-line 
    document.cookie = d.trim().split('=')[0] + '=;' + 'expires=Thu, 01 Jan 1970 00:00:00 UTC;';
  });
}

export const SIDEBAR_TOGGLE = 'SIDEBAR_TOGGLE';

export const SideBarToggle = (payload: any) => {
  return {
    type: SIDEBAR_TOGGLE,
    payload
  };
};

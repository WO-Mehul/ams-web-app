import { swagger } from "../../common/API";

export const LOGIN = 'LOGIN';
export const SIGN_UP = 'SIGN_UP';
export const SIGN_OUT = 'SIGN_OUT';
export const CREATE_PAYMENT = 'CREATE_PAYMENT';
export const CREATE_ADDRESS_INFO = 'CREATE_ADDRESS_INFO';
export const CREATE_PROFESSIONAL_EXPERIENCE = 'CREATE_PROFESSIONAL_EXPERIENCE';
export const CREATE_PROFESSIONAL_REFERENCE = 'CREATE_PROFESSIONAL_REFERENCE';
export const UPDATE_EMPLOYEE = 'UPDATE_EMPLOYEE';

export const LoginAPI = (dataObj: any) => {
    swagger.setAuthorization(dataObj.email, dataObj.password);
    return swagger.client.apis.auth.login()
        .then((response: any) => {
            if (response.status === 200) {
                swagger.setHeader({ 'x-access-token': response.body.token });
                if (response.body.superToken) {
                    swagger.setHeader({ 'x-super-access-token': response.body.superToken });
                }
                return {
                    type: LOGIN,
                    payload: response.body
                };
            }
        });
};
export const VerifyEmailAPI = (dataObj: any) => {
    return swagger.client.apis.auth.verifyEmail(dataObj)
        .then((response: any) => {
            return {
                type: LOGIN,
                payload: response.body
            };
        });
};
export const SignOutAPI = () => {
    return {
        type: SIGN_OUT
    };
};
export const SignUpAPI = (obj: any) => {
    return swagger.client.apis.auth.signUp({ id: 1 }, { requestBody: obj })
        .then((response: any) => {
            return {
                type: SIGN_UP,
                payload: response.body
            };
        });
};

export const CreatePaymentAPI = (obj: any) => {
    return swagger.client.apis.payments.addPayment({ id: 1 }, { requestBody: obj })
        .then((response: any) => {
            return {
                type: CREATE_PAYMENT,
                payload: response.body
            };
        });
};

export const CreateAddressInfoAPI = (obj: any) => {
    return swagger.client.apis.employeeAddressInfo.addEmployeeAddressInfo({ id: 1 }, { requestBody: obj })
        .then((response: any) => {
            return {
                type: CREATE_ADDRESS_INFO,
                payload: response.body
            };
        });
};

export const CreateProfessionalExperienceAPI = (obj: any) => {
    return swagger.client.apis.professionalExperience.updateProfessionalExperienceById({ id: obj.employee }, { requestBody: obj })
        .then((response: any) => {
            return {
                type: CREATE_PROFESSIONAL_EXPERIENCE,
                payload: response.body
            };
        });
};

export const CreateProfessionalReferenceAPI = (obj: any) => {
    return swagger.client.apis.professionalReference.addProfessionalReference({ id: 1 }, { requestBody: obj })
        .then((response: any) => {
            return {
                type: CREATE_PROFESSIONAL_REFERENCE,
                payload: response.body
            };
        });
};

export const UpdateEmployeeAPI = (obj: any) => {
    return swagger.client.apis.employees.updateEmployeeById({ id: obj.employee }, { requestBody: obj })
        .then((response: any) => {
            return {
                type: UPDATE_EMPLOYEE,
                payload: response.body
            };
        });
};

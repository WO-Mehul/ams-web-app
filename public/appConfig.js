/**
 * DO NOT MODIFY AND SAVE TO REPO
 * When Modifing this file use internal application processes.
 * This script is used to bootstrap the client application.
 * This script will check the document location to set up
 * the environment it will run based on the API Server
 * requirements.
 * This script is ment to die silently if there is an error,
 * use error handling with in the application to catch errors
 * before they get here.
 */
var AppConfig = {
    debug: true,
    isProd: false,
    port: 8080,
    service: {
        id: '',
        key: ''
    },
    timeout: 50000,
    basename: '/',
    host: '192.168.0.113'
}
